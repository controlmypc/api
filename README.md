If you wanna use this with Glitch:
1. Go to Tools > Import and Export on Glitch
2. Get the git url from there and clone it
3. Add this repo with `git remote add gitlab https://gitlab.com/controlmypc/api.git`

Now you can pull from Glitch:
- `git pull origin master`    
To push to Glitch you have to use a different branch, then merge in the Glitch console.
- `git checkout import`
- `git push origin import`

And pull from / push to GitLab:
- `git pull gitlab master`
- `Git push gitlab master`
